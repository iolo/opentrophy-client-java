package com.opentrophy.client;

import org.json.JSONObject;

public class OTUserSummary {

    private final OTUser user;
    // TODO: ...

    private OTUserSummary(JSONObject json) {
        user = OTUser.SERIALIZER.fromJson(json.optJSONObject("user"));
    }

    public OTUser getUser() {
        return user;
    }

    public static OTUserSummary fromJson(JSONObject json) {
        return new OTUserSummary(json);
    }
}
