package com.opentrophy.client;

import org.json.JSONObject;

public class OTAchievementSummary {

    private final OTAchievement achievement;
    // TODO: ...

    private OTAchievementSummary(JSONObject json) {
        achievement = OTAchievement.SERIALIZER.fromJson(json.optJSONObject("achievement"));
    }

    public OTAchievement getAchievement() {
        return achievement;
    }

    public static OTAchievementSummary fromJson(JSONObject json) {
        return new OTAchievementSummary(json);
    }
}
