package com.opentrophy.client;

import org.json.JSONObject;

import java.util.List;

public class OTLeaderboard {

    private final String id;
    private final String type;
    private final String status;
    private final String appId;

    private String title;
    private String subtitle;
    private String description;
    private String icon;
    private int displayOrder;
    private String scoreOrder;
    private String scoreFormat;

    public OTLeaderboard(String appId) {
        id = null;
        type = "leaderboard";
        status = "active";
        this.appId = appId;
    }

    private OTLeaderboard(final JSONObject json) {
        id = json.optString("_id");
        type = json.optString("type");
        status = json.optString("status");
        appId = json.optString("app");
        title = json.optString("title");
        subtitle = json.optString("subtitle");
        icon = json.optString("icon");
        description = json.optString("description");
        displayOrder = json.optInt("displayOrder");
        scoreOrder = json.optString("scoreOrder");
        scoreFormat = json.optString("scoreFormat");
    }

    //
    // getters
    //

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public String getAppId() {
        return appId;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public String getScoreOrder() {
        return scoreOrder;
    }

    public String getScoreFormat() {
        return scoreFormat;
    }

    //
    // settters
    //

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public void setScoreOrder(String scoreOrder) {
        this.scoreOrder = scoreOrder;
    }

    public void setScoreFormat(String scoreFormat) {
        this.scoreFormat = scoreFormat;
    }

    //
    //
    //

    @Override
    public String toString() {
        return "[OTLeaderboard:id=" + id + ",title=" + title + "]";
    }

    public static final OTJsonSerializer<OTLeaderboard> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTLeaderboard>() {
        @Override
        public JSONObject toJson(final OTLeaderboard obj) {
            return OTJson.object("_id", obj.id, "type", obj.type, "status", obj.status, "app", obj.appId,
                    "title", obj.title, "subtitle", obj.subtitle, "description", obj.description, "icon", obj.icon,
                    "displayOrder", obj.displayOrder, "scoreOrder", obj.scoreOrder, "scoreFormat", obj.scoreFormat);
        }

        @Override
        public OTLeaderboard fromJson(final JSONObject json) {
            return new OTLeaderboard(json);
        }
    };
}
