package com.opentrophy.client;

import org.json.JSONObject;

public class OTTag {

    private final String id;

    private OTTag(final JSONObject json) {
        id = json.optString("_id");
    }

    public String getId() {
        return id;
    }
//
    //
    //

    @Override
    public String toString() {
        return "[OPage:id=" + id + "]";
    }

    public static final OTJsonSerializer<OTTag> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTTag>() {
        @Override
        public JSONObject toJson(final OTTag obj) {
            return OTJson.object("_id", obj.id);
        }

        @Override
        public OTTag fromJson(final JSONObject json) {
            return new OTTag(json);
        }
    };
}
