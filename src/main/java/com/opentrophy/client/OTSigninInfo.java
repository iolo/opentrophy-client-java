package com.opentrophy.client;

import org.json.JSONException;
import org.json.JSONObject;

public class OTSigninInfo {

    private final String provider;
    private final String username;
    private final String password;
    private final boolean rememberMe;

    public OTSigninInfo(String provider, String username, String password, boolean rememberMe) {
        this.provider = provider;
        this.username = username;
        this.password = password;
        this.rememberMe = rememberMe;
    }

    //
    //
    //

    public JSONObject toJson() {
        return OTJson.object("provider", provider, "username", username, "password", password, "rememberMe", rememberMe);
    }

}
