package com.opentrophy.client;

import org.json.JSONObject;

public class OTLeaderboardSummary {

    private final OTLeaderboard leaderboard;
    // TODO: ...

    private OTLeaderboardSummary(JSONObject json) {
        leaderboard = OTLeaderboard.SERIALIZER.fromJson(json.optJSONObject("leaderboard"));
    }

    public OTLeaderboard getLeaderboard() {
        return leaderboard;
    }

    public static OTLeaderboardSummary fromJson(JSONObject json) {
        return new OTLeaderboardSummary(json);
    }
}
