package com.opentrophy.client;

import org.json.JSONObject;

public class OTPlayerSummary {

    private final OTPlayer player;
    // TODO: ...

    private OTPlayerSummary(JSONObject json) {
        player = OTPlayer.SERIALIZER.fromJson(json.optJSONObject("player"));
    }

    public OTPlayer getPlayer() {
        return player;
    }

    public static OTPlayerSummary fromJson(JSONObject json) {
        return new OTPlayerSummary(json);
    }
}
