package com.opentrophy.client;

import org.json.JSONObject;

public class OTActivity {

    private final String id;

    private OTActivity(final JSONObject json) {
        id = json.optString("_id");
    }

    public String getId() {
        return id;
    }
//
    //
    //

    @Override
    public String toString() {
        return "[OPage:id=" + id + "]";
    }

    public static final OTJsonSerializer<OTActivity> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTActivity>() {
        @Override
        public JSONObject toJson(final OTActivity obj) {
            return OTJson.object("_id", obj.id);
        }

        @Override
        public OTActivity fromJson(final JSONObject json) {
            return new OTActivity(json);
        }
    };
}
