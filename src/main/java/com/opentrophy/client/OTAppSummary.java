package com.opentrophy.client;

import org.json.JSONObject;

public class OTAppSummary {

    private final OTApp app;
    // TODO: ...

    private OTAppSummary(JSONObject json) {
        app = OTApp.SERIALIZER.fromJson(json.optJSONObject("app"));
    }

    public OTApp getApp() {
        return app;
    }

    public static OTAppSummary fromJson(JSONObject json) {
        return new OTAppSummary(json);
    }
}
