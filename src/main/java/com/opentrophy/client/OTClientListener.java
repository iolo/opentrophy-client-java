package com.opentrophy.client;

public interface OTClientListener {

    void onConnect(final OTApp app);

    void onDisconnect();

    void onSignin(final OTPlayer player);

    void onSignout();

    class Noop implements OTClientListener {

        @Override
        public void onConnect(final OTApp app) {
        }

        @Override
        public void onDisconnect() {
        }

        @Override
        public void onSignin(final OTPlayer user) {
        }

        @Override
        public void onSignout() {
        }

    }
}
