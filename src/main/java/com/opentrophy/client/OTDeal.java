package com.opentrophy.client;

import org.json.JSONObject;

public class OTDeal {

    private final String id;

    private OTDeal(final JSONObject json) {
        id = json.optString("_id");
    }

    public String getId() {
        return id;
    }
//
    //
    //

    @Override
    public String toString() {
        return "[OPage:id=" + id + "]";
    }

    public static final OTJsonSerializer<OTDeal> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTDeal>() {
        @Override
        public JSONObject toJson(final OTDeal obj) {
            return OTJson.object("_id", obj.id);
        }

        @Override
        public OTDeal fromJson(final JSONObject json) {
            return new OTDeal(json);
        }
    };
}
