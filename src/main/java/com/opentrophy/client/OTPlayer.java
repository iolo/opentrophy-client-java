package com.opentrophy.client;

import org.json.JSONObject;

public class OTPlayer {

    private final String id;
    private final String appId;
    private final String userId;

    private String displayName;
    private String icon;
    private int gold;
    private int point;
    private int score;
    private int level;
    private double lat;
    private double lng;

    public OTPlayer(String appId, String userId) {
        this.id = null;
        this.appId = appId;
        this.userId = userId;
    }

    public OTPlayer(JSONObject json) {
        this.id = json.optString("_id");
        this.appId = json.optString("app");
        this.userId = json.optString("user");
    }

    //
    // getters
    //

    public String getId() {
        return id;
    }

    public String getAppId() {
        return appId;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getIcon() {
        return icon;
    }

    public int getGold() {
        return gold;
    }

    public int getPoint() {
        return point;
    }

    public int getScore() {
        return score;
    }

    public int getLevel() {
        return level;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
//
    // setters
    //

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setLoc(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    //
    //
    //

    @Override
    public String toString() {
        return "[OTPlayer:id=" + id + ",app=" + appId + ",user=" + userId + "]";
    }

    public static final OTJsonSerializer<OTPlayer> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTPlayer>() {
        @Override
        public JSONObject toJson(final OTPlayer obj) {
            return OTJson.object("_id", obj.id, "app", obj.appId, "user", obj.userId,
                    "displayName", obj.displayName, "icon", obj.icon,
                    "gold", obj.gold, "point", obj.point, "score", obj.score, "level", obj.level,
                    "loc", OTJson.array(obj.lat, obj.lng));
        }

        @Override
        public OTPlayer fromJson(final JSONObject json) {
            return new OTPlayer(json);
        }
    };
}
