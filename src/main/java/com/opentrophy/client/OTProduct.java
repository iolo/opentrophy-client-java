package com.opentrophy.client;

import org.json.JSONObject;

public class OTProduct {

    private final String id;
    private final String type;
    private final String status;
    private final String appId;

    private String title;
    private String subtitle;
    private String description;
    private String icon;

    public OTProduct(OTApp app) {
        id = null;
        type = "product";
        status = "active";
        appId = app.getId();
    }

    private OTProduct(JSONObject json) {
        id = json.optString("_id");
        type = json.optString("type");
        status = json.optString("status");
        appId = json.optString("app");
        title = json.optString("title");
        subtitle = json.optString("subtitle");
        description = json.optString("description");
        icon = json.optString("icon");
    }

    //
    // getters
    //

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public String getAppId() {
        return appId;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    //
    // setters
    //

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    //
    //
    //

    public static final OTJsonSerializer<OTProduct> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTProduct>() {

        @Override
        public JSONObject toJson(OTProduct obj) {
            return OTJson.object("_id", obj.id, "type", obj.type, "status", obj.status, "app", obj.appId,
                    "title", obj.title, "subtitle", obj.subtitle, "description", obj.description, "icon", obj.icon);
        }

        @Override
        public OTProduct fromJson(JSONObject json) {
            return new OTProduct(json);
        }
    };

}
