package com.opentrophy.client;

import org.json.JSONObject;

public class OTGroup {

    private final String id;
    private final String type;
    private final String status;
    private final String appId;
    private final String userId;

    private String title;
    private String subtitle;
    private String description;
    private String icon;

    public OTGroup(String appId, String userId) {
        this.id = null;
        this.type = "group";
        this.status = "active";
        this.appId = appId;
        this.userId = userId;
    }

    private OTGroup(final JSONObject obj) {
        id = obj.optString("id");
        type = obj.optString("type");
        status = obj.optString("status");
        appId = obj.optString("app");
        userId = obj.optString("user");
        title = obj.optString("title");
        subtitle = obj.optString("subtitle");
        description = obj.optString("description");
        icon = obj.optString("icon");
    }

    //
    // getters
    //

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public String getAppId() {
        return appId;
    }

    public String getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    //
    // setters
    //

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    //
    //
    //

    @Override
    public String toString() {
        return "[OTGroup:id=" + id + ",title=" + title + "]";
    }

    public static final OTJsonSerializer<OTGroup> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTGroup>() {
        @Override
        public JSONObject toJson(final OTGroup obj) {
            return OTJson.object("_id", obj.id, "type", obj.type, "status", obj.status,
                    "app", obj.appId, "user", obj.userId,
                    "title", obj.title, "subtitle", obj.subtitle, "description", obj.description, "icon", obj.icon);
        }

        @Override
        public OTGroup fromJson(final JSONObject json) {
            return new OTGroup(json);
        }
    };

}
