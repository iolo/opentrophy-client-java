package com.opentrophy.client;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OTProgress {

    private final String id;
    private final OTPlayer player;
    private final String playerId;
    private final String achievementId;
    private final int value;
    private final Date createdAt;

    public OTProgress(String playerId, String achievementId, int value) {
        this.id = null;
        this.player = null;
        this.playerId = playerId;
        this.achievementId = achievementId;
        this.value = value;
        this.createdAt = new Date();
    }

    private OTProgress(JSONObject json) {
        id = json.optString("_id");
        if (json.opt("player") instanceof String) {
            player = null;
            playerId = json.optString("player");
        } else {
            player = OTPlayer.SERIALIZER.fromJson(json.optJSONObject("player"));
            playerId = player.getId();
        }
        achievementId = json.optString("achievement");
        value = json.optInt("value");
        createdAt = OTUtils.toDate(json.optString("createdAt"));
    }

    //
    // getters
    //

    public String getId() {
        return id;
    }

    public OTPlayer getPlayer() {
        return player;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getAchievementId() {
        return achievementId;
    }

    public int getValue() {
        return value;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    //
    //
    //

    @Override
    public String toString() {
        return "[OTScore:player=" + playerId + ",achievement=" + achievementId + ",value=" + value + "]";
    }

    public static final OTJsonSerializer<OTProgress> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTProgress>() {
        @Override
        public JSONObject toJson(final OTProgress obj) {
            return OTJson.object("_id", obj.id, "player", obj.playerId, "achievement", obj.achievementId, "value", obj.value);
        }

        @Override
        public OTProgress fromJson(final JSONObject json) {
            return new OTProgress(json);
        }
    };
}
