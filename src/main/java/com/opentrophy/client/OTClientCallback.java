package com.opentrophy.client;

public interface OTClientCallback<T> {

    void onSuccess(T data);

    void onError(OTClientError error);

    static class Noop<T> implements OTClientCallback<T> {

        @Override
        public void onSuccess(T data) {
        }

        @Override
        public void onError(OTClientError error) {
        }

    }

    static class Delegate {
        public static final void success(OTClientCallback callback, Object data) {
            if (callback != null) {
                callback.onSuccess(data);
            }
        }

        public static final void error(OTClientCallback callback, OTClientError error) {
            if (callback != null) {
                callback.onError(error);
            }
        }
    }
}
