package com.opentrophy.client;

import org.json.JSONObject;

public class OTUser {

    private final String id;
    private final String type;
    private final String status;
    //private final String provider;
    //private final String username;
    //private final String passwordPlain;
    //private final String[] roles;

    private String email;
    private String displayName;
    private String icon;
    private String firstName;
    private String lastName;
    private String url;
    private String location;
    private String description;

    public OTUser() {
        this.id = null;
        this.type = "user";
        this.status = "active";
    }

    private OTUser(JSONObject obj) {
        id = obj.optString("id");
        type = obj.optString("type");
        status = obj.optString("status");
        displayName = obj.optString("displayName");
        icon = obj.optString("icon");
        firstName = obj.optString("firstName");
        lastName = obj.optString("lastName");
        url = obj.optString("url");
        location = obj.optString("location");
        description = obj.optString("description");
    }

    //
    // getters
    //

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public String getEmail() {
        return email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getIcon() {
        return icon;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUrl() {
        return url;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    //
    // setters
    //

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //
    //
    //

    @Override
    public String toString() {
        return "[OTUser:id=" + id + ",displayName=" + displayName + "]";
    }

    public static final OTJsonSerializer<OTUser> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTUser>() {
        @Override
        public JSONObject toJson(final OTUser obj) {
            return OTJson.object("_id", obj.id, "type", obj.type, "status", obj.status,
                    "displayName", obj.displayName, "icon", obj.icon,
                    "firstName", obj.firstName, "lastName", obj.lastName,
                    "url", obj.url, "location", obj.location, "description", obj.description);
        }

        @Override
        public OTUser fromJson(final JSONObject json) {
            return new OTUser(json);
        }
    };
}
