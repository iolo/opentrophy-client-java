package com.opentrophy.client;

import org.json.JSONObject;

import java.util.List;

public class OTAchievement {

    private final String id;
    private final String type;
    private final String status;
    private final String appId;

    private String title;
    private String subtitle;
    private String description;
    private String icon;
    private int displayOrder;

    public OTAchievement(String appId) {
        id = null;
        type = "achievement";
        status = "active";
        this.appId = appId;
    }

    private OTAchievement(final JSONObject json) {
        id = json.optString("id");
        type = json.optString("type");
        status = json.optString("status");
        appId = json.optString("appId");
        title = json.optString("title");
        subtitle = json.optString("subtitle");
        description = json.optString("description");
        icon = json.optString("icon");
        displayOrder = json.optInt("displayOrder");
    }

    //
    // getters
    //

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public String getAppId() {
        return appId;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    //
    // setters
    //

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    //
    //
    //

    @Override
    public String toString() {
        return "[OTAchievement:id=" + id + ",title=" + title + "]";
    }

    public static final OTJsonSerializer<OTAchievement> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTAchievement>() {
        @Override
        public JSONObject toJson(final OTAchievement obj) {
            return OTJson.object("_id", obj.id, "type", obj.type, "status", obj.status, "app", obj.appId,
                    "title", obj.title, "subtitle", obj.subtitle, "description", obj.description, "icon", obj.icon,
                    "displayOrder", obj.displayOrder);
        }

        @Override
        public OTAchievement fromJson(final JSONObject json) {
            return new OTAchievement(json);
        }
    };
}
