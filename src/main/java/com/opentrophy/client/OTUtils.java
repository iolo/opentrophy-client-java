package com.opentrophy.client;

import java.io.*;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OTUtils {

    private static final DateFormat ISO8601_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private static final Date FALLBACK_DATE = new Date(0);

    private static final int FALLBACK_INT = 0;

    private static final String ENCODING = "UTF-8";

    public static Date toDate(String str, Date fallback) {
        try {
            return ISO8601_FORMAT.parse(str);
        } catch (Exception e) {
            return fallback;
        }
    }

    public static Date toDate(String str) {
        return toDate(str, FALLBACK_DATE);
    }

    public static String toString(Date date) {
        return ISO8601_FORMAT.format(date);
    }

    public static int toInt(String str, int fallback) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            return fallback;
        }
    }

    public static int toInt(String str) {
        return toInt(str, FALLBACK_INT);
    }

    public static byte[] toByteArray(InputStream in) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int count;
        while ((count = in.read(buf)) != -1) {
            out.write(buf, 0, count);
        }
        return out.toByteArray();
    }

    public static String toString(int n) {
        return Integer.toString(n);
    }

    public static void close(InputStream in) {
        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
                // ignore!
            }
        }
    }

    public static void close(OutputStream out) {
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
                // ignore!
            }
        }
    }

    public static String encodeURIComponent(String uri) {
        try {
            return URLEncoder.encode(uri, ENCODING);
        } catch (UnsupportedEncodingException e) {
            return uri;
        }
    }

    public static String encodeQueryParams(Map<String, Object> params) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            String key = encodeURIComponent(entry.getKey());
            String value = encodeURIComponent(entry.getValue().toString());
            builder.append(key).append("=").append(value).append("&");
        }
        builder.setLength(builder.length() - 1);
        return builder.toString();
    }

    public static Map<String, Object> params(Object... args) {
        Map<String, Object> map = new HashMap<String, Object>();
        for (int i = 0, len = args.length; i < len; i += 2) {
            Object key = args[i];
            Object value = args[i + 1];
            if (key != null && value != null) {
                map.put(key.toString(), value);
            }
        }
        return map;
    }

    public static String dump(Object o) {
        StringBuffer buffer = new StringBuffer();
        Class oClass = o.getClass();
        if (oClass.isArray()) {
            buffer.append("Array: ");
            buffer.append("[");
            for (int i = 0; i < Array.getLength(o); i++) {
                Object value = Array.get(o, i);
                if (value.getClass().isPrimitive() ||
                        value.getClass() == java.lang.Long.class ||
                        value.getClass() == java.lang.Integer.class ||
                        value.getClass() == java.lang.Boolean.class ||
                        value.getClass() == java.lang.String.class ||
                        value.getClass() == java.lang.Double.class ||
                        value.getClass() == java.lang.Short.class ||
                        value.getClass() == java.lang.Byte.class
                        ) {
                    buffer.append(value);
                    if (i != (Array.getLength(o) - 1)) buffer.append(",");
                } else {
                    buffer.append(dump(value));
                }
            }
            buffer.append("]\n");
        } else {
            buffer.append("Class: " + oClass.getName());
            buffer.append("{\n");
            while (oClass != null) {
                Field[] fields = oClass.getDeclaredFields();
                for (int i = 0; i < fields.length; i++) {
                    fields[i].setAccessible(true);
                    buffer.append(fields[i].getName());
                    buffer.append("=");
                    try {
                        Object value = fields[i].get(o);
                        if (value != null) {
                            if (value.getClass().isPrimitive() ||
                                    value.getClass() == java.lang.Long.class ||
                                    value.getClass() == java.lang.String.class ||
                                    value.getClass() == java.lang.Integer.class ||
                                    value.getClass() == java.lang.Boolean.class ||
                                    value.getClass() == java.lang.Double.class ||
                                    value.getClass() == java.lang.Short.class ||
                                    value.getClass() == java.lang.Byte.class
                                    ) {
                                buffer.append(value);
                            } else {
                                buffer.append(dump(value));
                            }
                        }
                    } catch (IllegalAccessException e) {
                        buffer.append(e.getMessage());
                    }
                    buffer.append("\n");
                }
                oClass = oClass.getSuperclass();
            }
            buffer.append("}\n");
        }
        return buffer.toString();
    }
}
