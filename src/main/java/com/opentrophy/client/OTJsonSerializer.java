package com.opentrophy.client;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public interface OTJsonSerializer<T> {

    JSONObject toJson(final T obj);

    JSONArray toJson(final List<T> objArray);

    T fromJson(final JSONObject json);

    List<T> fromJson(final JSONArray jsonArray);

    abstract class WithArraySupport<T> implements OTJsonSerializer<T> {

        @Override
        public JSONArray toJson(final List<T> objArray) {
            JSONArray result = new JSONArray();
            for (T obj : objArray) {
                result.put(toJson(obj));
            }
            return result;
        }

        @Override
        public List<T> fromJson(final JSONArray jsonArray) {
            final int len = jsonArray.length();
            final List<T> result = new ArrayList<T>(len);
            for (int i = 0; i < len; i++) {
                result.add(fromJson(jsonArray.optJSONObject(i)));
            }
            return result;
        }

    }
}
