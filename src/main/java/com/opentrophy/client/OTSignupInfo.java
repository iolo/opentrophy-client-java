package com.opentrophy.client;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class OTSignupInfo {

    private final OTSigninInfo signinInfo;
    private final OTUser user;
    private final boolean signinNow;

    public OTSignupInfo(OTSigninInfo signinInfo, OTUser user, boolean signinNow) {
        this.signinInfo = signinInfo;
        this.user = user;
        this.signinNow = signinNow;
    }

    public OTSigninInfo getSigninInfo() {
        return signinInfo;
    }

    public boolean isSigninNow() {
        return signinNow;
    }

    public OTUser getUser() {
        return user;
    }

    public JSONObject toJson() {
        JSONObject json = signinInfo.toJson();
        try {
            if (user != null) {
                JSONObject userJson = OTUser.SERIALIZER.toJson(user);
                for (Iterator it = userJson.keys(); it.hasNext(); ) {
                    String key = (String) it.next();
                    json.put(key, userJson.opt(key));
                }
            }
            json.put("signinNow", signinNow);
        } catch (JSONException e) {
            throw new OTException(e);
        }
        return json;
    }
}
