package com.opentrophy.client;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Collection;
import java.util.Map;

public class OTJson {

    public static Object decode(final byte[] data) {
        return parse(new String(data));
    }

    public static byte[] encode(final Object obj) {
        return stringify(obj).getBytes();
    }

    public static Object parse(final String str) {
        try {
            return new JSONTokener(str).nextValue();
        } catch (JSONException e) {
            throw new OTException("json parse error", e);
        }
    }

    public static String stringify(final Object obj) {
        if (obj instanceof JSONObject || obj instanceof JSONArray) {
            return obj.toString();
        }
        if (obj instanceof Map) {
            return new JSONObject((Map) obj).toString();
        }
        if (obj instanceof Collection) {
            return new JSONArray((Collection) obj).toString();
        }
        throw new OTException("json stringify error");
    }

    public static JSONObject object(final Object... args) {
        try {
            final JSONObject result = new JSONObject();
            for (int i = 0, len = args.length - 1; i < len; i += 2) {
                Object key = args[i];
                Object value = args[i + 1];
                if (key != null && value != null) {
                    result.put(key.toString(), value);
                }
            }
            return result;
        } catch (JSONException e) {
            throw new OTException("json object build error", e);
        }
    }

    public static JSONArray array(final Object... args) {
        try {
            final JSONArray result = new JSONArray();
            for (int i = 0, len = args.length; i < len; i++) {
                result.put(i, args[i]);
            }
            return result;
        } catch (JSONException e) {
            throw new OTException("json array build error", e);
        }
    }
}
