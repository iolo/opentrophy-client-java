package com.opentrophy.client;

public class OTException extends RuntimeException {

    public OTException() {
        super();
    }

    public OTException(String message) {
        super(message);
    }

    public OTException(String message, Throwable cause) {
        super(message, cause);
    }

    public OTException(Throwable cause) {
        super(cause);
    }
}
