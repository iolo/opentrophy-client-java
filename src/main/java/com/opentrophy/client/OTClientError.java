package com.opentrophy.client;

import org.json.JSONObject;

public class OTClientError {

    private int status;
    private String message;
    private Object cause;

    public OTClientError(int status, String message, Object cause) {
        this.status = status;
        this.message = message;
        this.cause = cause;
    }

    public OTClientError(JSONObject json) {
        this.status = json.optInt("status", 0);
        this.message = json.optString("message", "OpenTrophy Client Error");
        this.cause = json.opt("cause");
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Object getCause() {
        return cause;
    }

    @Override
    public String toString() {
        return "[OTClientError:status=" + status + ",message=" + message + "]";
    }

    //---------------------------------------------------------------

    public static OTClientError fromJson(JSONObject json) {
        return new OTClientError(json);
    }
}
