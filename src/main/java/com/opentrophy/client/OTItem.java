package com.opentrophy.client;

import org.json.JSONObject;

public class OTItem {

    private final String id;
    private final String productId;
    private final int quantity;

    private OTItem(final JSONObject json) {
        id = json.optString("_id");
        productId = json.optString("product");
        quantity = json.optInt("quantity");
    }

    public String getId() {
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }
//
    //
    //

    @Override
    public String toString() {
        return "[OTItem:id=" + id + ",productId=" + productId + "]";
    }

    public static final OTJsonSerializer<OTItem> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTItem>() {
        @Override
        public JSONObject toJson(final OTItem obj) {
            return OTJson.object("_id", obj.id,
                    "product", obj.productId, "quantity", obj.quantity);
        }

        @Override
        public OTItem fromJson(final JSONObject json) {
            return new OTItem(json);
        }
    };
}
