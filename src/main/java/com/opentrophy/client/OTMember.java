package com.opentrophy.client;

import org.json.JSONObject;

public class OTMember {

    private final String id;
    private final String type;
    private final String status;
    private final String groupId;
    private final String userId;

    public OTMember(String groupId, String userId) {
        this.id = null;
        this.type = "member";
        this.status = "active";
        this.groupId = groupId;
        this.userId = userId;
    }

    private OTMember(final JSONObject obj) {
        id = obj.optString("_id");
        type = obj.optString("type");
        status = obj.optString("status");
        groupId = obj.optString("group");
        userId = obj.optString("user");
    }

    //
    // getters
    //

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getUserId() {
        return userId;
    }

    //
    //
    //

    @Override
    public String toString() {
        return "[OTMember:id=" + id + ",group=" + groupId + ",user=" + userId + "]";
    }

    public static final OTJsonSerializer<OTMember> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTMember>() {
        @Override
        public JSONObject toJson(final OTMember obj) {
            return OTJson.object("_id", obj.id, "type", obj.type, "status", obj.status,
                    "group", obj.groupId, "user", obj.userId);
        }

        @Override
        public OTMember fromJson(final JSONObject json) {
            return new OTMember(json);
        }
    };
}
