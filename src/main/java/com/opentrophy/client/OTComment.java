package com.opentrophy.client;

import org.json.JSONObject;

public class OTComment {

    private final String id;

    private OTComment(final JSONObject json) {
        id = json.optString("_id");
    }

    public String getId() {
        return id;
    }

    //
    //
    //

    @Override
    public String toString() {
        return "[OTComment:id=" + id + "]";
    }

    public static final OTJsonSerializer<OTComment> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTComment>() {
        @Override
        public JSONObject toJson(final OTComment obj) {
            return OTJson.object("_id", obj.id);
        }

        @Override
        public OTComment fromJson(final JSONObject json) {
            return new OTComment(json);
        }
    };
}
