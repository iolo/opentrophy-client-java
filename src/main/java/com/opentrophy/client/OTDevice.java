package com.opentrophy.client;

import org.json.JSONObject;

public class OTDevice {
    private String id;
    private String playerId;
    private String udid;
    private String model;

    public OTDevice() {
    }

    public OTDevice(String playerId, String udid, String model) {
        this.id = null;
        this.playerId = playerId;
        this.udid = udid;
        this.model = model;
    }

    private OTDevice(final JSONObject json) {
        id = json.optString("_id");
        playerId = json.optString("player");
        udid = json.optString("udid");
        model = json.optString("model");
    }

    public String getId() {
        return id;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getUdid() {
        return udid;
    }

    public String getModel() {
        return model;
    }

    //
    //
    //

    @Override
    public String toString() {
        return "[OTDevice: playerId=" + playerId + ",udid=" + udid + ",model=" + model + "]";
    }

    public static final OTJsonSerializer<OTDevice> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTDevice>() {
        @Override
        public JSONObject toJson(final OTDevice device) {
            return OTJson.object("id", device.id, "player", device.playerId, "udid", device.udid, "model", device.model);
        }

        @Override
        public OTDevice fromJson(final JSONObject json) {
            return new OTDevice(json);
        }
    };
}
