package com.opentrophy.client;

import org.json.JSONObject;

import java.util.List;

public class OTLeaderboardScores {

    private final OTLeaderboard leaderboard;
    private final List<OTScore> scores;

    private OTLeaderboardScores(final JSONObject json) {
        leaderboard = OTLeaderboard.SERIALIZER.fromJson(json.optJSONObject("leaderboard"));
        scores = OTScore.SERIALIZER.fromJson(json.optJSONArray("scores"));
    }

    public OTLeaderboard getLeaderboard() {
        return leaderboard;
    }

    public List<OTScore> getScores() {
        return scores;
    }

    public static OTLeaderboardScores fromJson(JSONObject json) {
        return new OTLeaderboardScores(json);
    }
}
