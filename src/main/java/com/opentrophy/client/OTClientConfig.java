package com.opentrophy.client;

public class OTClientConfig {

    private static final String DEF_CONNECT_URL = "https://api.opentrophy.com/auth/bearer";
    private static final String DEF_API_URL = "https://api.opentrophy.com/api/v1";

    private final String key;
    private final String secret;
    private final String connectUrl;
    private final String apiUrl;
    private final String deviceUdid;
    private final String deviceModel;

    public OTClientConfig(String key, String secret, String connectUrl, String apiUrl, String deviceUdid, String deviceModel) {
        this.key = key;
        this.secret = secret;
        this.connectUrl = connectUrl;
        this.apiUrl = apiUrl;
        this.deviceUdid = deviceUdid;
        this.deviceModel = deviceModel;
    }

    public OTClientConfig(String key, String secret, String connectUrl, String apiUrl) {
        this(key, secret, connectUrl, apiUrl, null, null);
    }

    public OTClientConfig(String key, String secret) {
        this(key, secret, DEF_CONNECT_URL, DEF_API_URL);
    }

    public String getKey() {
        return key;
    }

    public String getSecret() {
        return secret;
    }

    public String getConnectUrl() {
        return connectUrl;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public String getDeviceUdid() {
        return deviceUdid;
    }

    public String getDeviceModel() {
        return deviceModel;
    }
}
