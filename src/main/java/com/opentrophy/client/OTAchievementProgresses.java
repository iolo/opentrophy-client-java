package com.opentrophy.client;

import org.json.JSONObject;

import java.util.List;

public class OTAchievementProgresses {

    private final OTAchievement achievement;
    private final List<OTProgress> progresses;

    private OTAchievementProgresses(JSONObject json) {
        achievement = OTAchievement.SERIALIZER.fromJson(json.optJSONObject("achievement"));
        progresses = OTProgress.SERIALIZER.fromJson(json.optJSONArray("progresses"));
    }

    public OTAchievement getAchievement() {
        return achievement;
    }

    public List<OTProgress> getProgresses() {
        return progresses;
    }

    public static OTAchievementProgresses fromJson(JSONObject json) {
        return new OTAchievementProgresses(json);
    }
}
