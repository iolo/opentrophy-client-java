package com.opentrophy.client;

import org.json.JSONObject;

public class OTTicket {

    private final String id;

    private OTTicket(final JSONObject json) {
        id = json.optString("_id");
    }

    public String getId() {
        return id;
    }
//
    //
    //

    @Override
    public String toString() {
        return "[OPage:id=" + id + "]";
    }

    public static final OTJsonSerializer<OTTicket> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTTicket>() {
        @Override
        public JSONObject toJson(final OTTicket obj) {
            return OTJson.object("_id", obj.id);
        }

        @Override
        public OTTicket fromJson(final JSONObject json) {
            return new OTTicket(json);
        }
    };
}
