package com.opentrophy.client;

import org.json.JSONObject;

public class OTApp {

    private final String id;
    private final String type;
    private final String status;
    private final String userId;

    private String title;
    private String subtitle;
    private String description;
    private String icon;

    public OTApp(String userId) {
        this.id = null;
        this.type = "app";
        this.status = "active";
        this.userId = userId;
    }

    private OTApp(JSONObject obj) {
        id = obj.optString("_id");
        type = obj.optString("type");
        status = obj.optString("status");
        userId = obj.optString("user");
        title = obj.optString("title");
        subtitle = obj.optString("subtitle");
        icon = obj.optString("icon");
        description = obj.optString("description");
    }

    //
    // getters
    //

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public String getUserId() {
        return userId;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    //
    // setters
    //

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
//
    //
    //

    @Override
    public String toString() {
        return "[OTApp:id=" + id + ",title=" + title + "]";
    }

    public static final OTJsonSerializer<OTApp> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTApp>() {
        @Override
        public JSONObject toJson(final OTApp obj) {
            return OTJson.object("_id", obj.id, "type", obj.type, "status", obj.status, "user", obj.userId,
                    "title", obj.title, "subtitle", obj.subtitle, "description", obj.description, "icon", obj.icon);
        }

        @Override
        public OTApp fromJson(final JSONObject json) {
            return new OTApp(json);
        }
    };

}
