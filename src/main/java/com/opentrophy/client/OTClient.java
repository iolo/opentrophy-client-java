package com.opentrophy.client;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OTClient {

    private static final Logger L = LoggerFactory.getLogger(OTClient.class);

    private final List<OTClientListener> listeners = new ArrayList<OTClientListener>();

    private final OTClientConfig config;

    private JSONObject bearer;
    private OTApp currentApp;
    private OTPlayer currentPlayer;

    public OTClient(OTClientConfig config) {
        this.config = config;
    }

    //
    //
    //

    public void addListener(OTClientListener listener) {
        this.listeners.add(listener);
    }

    public void removeListener(OTClientListener listener) {
        this.listeners.remove(listener);
    }

    //
    // privates
    //

    protected void _notifyConnect() {
        if (L.isDebugEnabled()) {
            L.debug("***otconnect***");
        }
        for (OTClientListener listener : listeners) {
            listener.onConnect(currentApp);
        }
    }

    protected void _notifyDisconnect() {
        if (L.isDebugEnabled()) {
            L.debug("***otdisconnect***");
        }
        for (OTClientListener listener : listeners) {
            listener.onDisconnect();
        }
    }

    protected void _notifySignin() {
        if (L.isDebugEnabled()) {
            L.debug("***otsignin***");
        }
        for (OTClientListener listener : listeners) {
            listener.onSignin(currentPlayer);
        }
    }

    protected void _notifySignout() {
        if (L.isDebugEnabled()) {
            L.debug("***otsignout***");
        }
        for (OTClientListener listener : listeners) {
            listener.onSignout();
        }
    }

    private static final String _ME = "me";

    private static String _me(String id) {
        return (id != null) ? id : _ME;
    }

    private static void _passSuccess(OTClientCallback callback, Object data) {
        if (callback != null) {
            callback.onSuccess(data);
        }
    }

    private static void _passError(OTClientCallback callback, OTClientError error) {
        if (callback != null) {
            callback.onError(error);
        }
    }

    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String PUT = "PUT";
    private static final String DELETE = "DELETE";

    private void _request(String method, String path, JSONObject data, Map<String, Object> params, OTClientCallback callback) {
        if (path != null && bearer == null) {
            _passError(callback, new OTClientError(403, "forbidden by client", null));
            return;
        }

        //CookieManager cookieManager = new CookieManager();
        //CookieHandler.setDefault(cookieManager);

        HttpURLConnection conn = null;
        InputStream in = null;
        OutputStream out = null;
        try {
            path = (path != null) ? config.getApiUrl() + path : config.getConnectUrl();
            if (params != null && !params.isEmpty()) {
                path += (path.indexOf('?') < 0 ? '?' : '&') + OTUtils.encodeQueryParams(params);
            }

            if (L.isDebugEnabled()) {
                L.debug("request: method=" + method + ",path=" + path);
            }

            URL url = new URL(path);
            conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setRequestMethod(method);
            if (bearer != null) {
                conn.setRequestProperty("Authorization", "Bearer " + bearer.optString("access_token"));
            }
            conn.setRequestProperty("Accept", "application/json");

            if (data != null) {
                conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                conn.setDoOutput(true);
                byte[] reqBytes = OTJson.encode(data);
                conn.setFixedLengthStreamingMode(reqBytes.length);
                out = new BufferedOutputStream(conn.getOutputStream());
                out.write(reqBytes);
                out.flush();
            }

            int status = conn.getResponseCode();
            //headers = conn.getHeaderFields();

            in = new BufferedInputStream(conn.getInputStream());

            byte[] resBytes = null;
            int resLen = conn.getContentLength();
            if (resLen <= 0) {
                if (status != HttpURLConnection.HTTP_NO_CONTENT) {
                    resBytes = OTUtils.toByteArray(in);
                }
            } else {
                resBytes = new byte[resLen];
                in.read(resBytes);
            }

            JSONObject result = null;
            if (resBytes != null) {
                result = (JSONObject) OTJson.decode(resBytes);
            }

            if (status >= 200 && status < 300) {
                if (L.isDebugEnabled()) {
                    L.debug("request ok: status=" + status + ",result=" + result);
                }
                _passSuccess(callback, result);
            } else {
                OTClientError error;
                if (result != null && result.has("error")) {
                    error = OTClientError.fromJson(result.optJSONObject("error"));
                } else {
                    error = new OTClientError(status, conn.getResponseMessage(), result.toString());
                }
                if (L.isDebugEnabled()) {
                    L.debug("request err: error=" + error);
                }
                _passError(callback, error);
            }
        } catch (Throwable t) {
            //_notifyDisconnect();
            OTClientError error = new OTClientError(0, t.getMessage(), t);
            if (L.isDebugEnabled()) {
                L.debug("request err: error=" + error);
            }
            _passError(callback, error);
        } finally {
            OTUtils.close(in);
            OTUtils.close(out);
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    private void _request(String method, String path, JSONObject data, OTClientCallback callback) {
        _request(method, path, data, null, callback);
    }

    private void _request(String method, String path, OTClientCallback callback) {
        _request(method, path, null, null, callback);
    }

    private void _request(String method, OTClientCallback callback) {
        _request(method, null, null, null, callback);
    }

    private class _Resource<T> {
        private final OTJsonSerializer<T> serializer;
        private final String plural;
        private final String singular;
        private final String path;

        public _Resource(OTJsonSerializer<T> serializer, String plural, String singular) {
            this.serializer = serializer;
            this.plural = plural;
            this.singular = singular;
            path = "/" + plural;
        }

        public void list(Map<String, Object> params, OTClientCallback<List<T>> callback) {
            _request(GET, path, null, params, new JsonPluralCallback<T>(serializer, plural, callback));
        }

        public void load(String id, OTClientCallback<T> callback) {
            _request(GET, path + "/" + id, null, new JsonSingularCallback<T>(serializer, singular, callback));
        }

        public void create(T data, OTClientCallback<T> callback) {
            _request(POST, path, serializer.toJson(data), new JsonSingularCallback<T>(serializer, singular, callback));
        }

        public void update(String id, T data, OTClientCallback<T> callback) {
            _request(PUT, path + "/" + id, serializer.toJson(data), new JsonSingularCallback<T>(serializer, singular, callback));
        }

        public void destroy(String id, OTClientCallback callback) {
            _request(DELETE, path + "/" + id, callback);
        }

    }

    //
    // getters
    //

    public OTPlayer getCurrentPlayer() {
        return currentPlayer;
    }

    public OTApp getCurrentApp() {
        return currentApp;
    }

    //
    // connect
    //

    public void connect(final OTClientCallback<OTApp> callback) {
        JSONObject data = OTJson.object("grant_type", "password", "key", config.getKey(), "secret", config.getSecret(),
                "device", OTJson.object("udid", config.getDeviceUdid(), "model", config.getDeviceModel()));

        _request(POST, null, data, new OTClientCallback<JSONObject>() {
            @Override
            public void onSuccess(JSONObject data) {
                bearer = data;
                currentApp = OTApp.SERIALIZER.fromJson(data.optJSONObject("app"));
                _notifyConnect();
                _passSuccess(callback, currentApp);
            }

            @Override
            public void onError(OTClientError error) {
                bearer = null;
                currentApp = null;
                _passError(callback, error);
            }
        });
    }

    public void disconnect(OTClientCallback callback) {
        _request(DELETE, callback);
        bearer = null;
        currentApp = null;
        currentPlayer = null;
        _notifySignout();
        _notifyDisconnect();
    }

    public void ping(final OTClientCallback callback) {
        _request(GET, callback);
    }

    //
    // auth
    //

    public void signup(final OTSignupInfo signupInfo, final OTClientCallback callback) {
        JSONObject data = signupInfo.toJson();

        _request(POST, "/auth/signup", data, new OTClientCallback<JSONObject>() {
            @Override
            public void onSuccess(JSONObject data) {
                // autor-signin
                if (signupInfo.isSigninNow()) {
                    currentPlayer = OTPlayer.SERIALIZER.fromJson(data.optJSONObject("player"));
                    _notifySignin();
                    _passSuccess(callback, currentPlayer);
                    return;
                }
                _passSuccess(callback, data);
            }

            @Override
            public void onError(OTClientError error) {
                _passError(callback, error);
            }
        });
    }

    public void signdown(String password, final OTClientCallback callback) {
        JSONObject data = OTJson.object("password", password);
        _request(DELETE, "/auth/signdown", data, new OTClientCallback() {
            @Override
            public void onSuccess(Object data) {
                currentPlayer = null;
                _notifySignout();
                _passSuccess(callback, data);
            }

            @Override
            public void onError(OTClientError error) {
                _passError(callback, error);
            }
        });
    }

    public void signin(final OTSigninInfo signinInfo, final OTClientCallback<OTPlayer> callback) {
        JSONObject data = signinInfo.toJson();

        _request(POST, "/auth/signin", data, null, new OTClientCallback<JSONObject>() {
            @Override
            public void onSuccess(JSONObject data) {
                currentPlayer = OTPlayer.SERIALIZER.fromJson(data.optJSONObject("player"));
                _notifySignin();
                _passSuccess(callback, currentPlayer);
            }

            @Override
            public void onError(OTClientError error) {
                currentPlayer = null;
                _passError(callback, error);
            }
        });
    }

    public void signout(final OTClientCallback callback) {
        currentPlayer = null;
        _notifySignout();
        _request(DELETE, "/auth/signout", callback);
    }


    public void recoverAccountByEmail(String email, OTClientCallback callback) {
        JSONObject data = OTJson.object("email", email);
        _request(POST, "/auth/recover", data, callback);
    }

    public void requestActivateAccountByEmail(String email, OTClientCallback callback) {
        JSONObject data = OTJson.object("email", email);
        _request(POST, "/auth/activate", data, callback);
    }

    public void activateAccountByToken(String token, OTClientCallback callback) {
        JSONObject data = OTJson.object("token", token);
        _request(PUT, "/auth/activate", data, callback);
    }

    public void requestResetPasswordByEmail(String email, OTClientCallback callback) {
        JSONObject data = OTJson.object("email", email);
        _request(POST, "/auth/password", data, callback);
    }

    public void resetPasswordByToken(String token, OTClientCallback callback) {
        JSONObject data = OTJson.object("token", token);
        _request(PUT, "/auth/password", data, callback);
    }

    //
    // common
    //

    public void echo(String method, JSONObject data, Map<String, Object> params, final OTClientCallback callback) {
        _request(method, "/common/echo", data, params, callback);
    }

    public void search(String q, String type, int skip, int limit, final OTClientCallback callback) {
        Map<String, Object> params = OTUtils.params("q", q, "type", type, "skip", skip, "limit", limit);
        _request(GET, "/common/search", null, params, callback);
    }

    //
    // low-level RESTful resources
    //

    public final _Resource<OTAchievement> achievements = new _Resource<OTAchievement>(OTAchievement.SERIALIZER, "achievements", "achievement");
    public final _Resource<OTActivity> activities = new _Resource<OTActivity>(OTActivity.SERIALIZER, "activities", "activity");
    public final _Resource<OTApp> apps = new _Resource<OTApp>(OTApp.SERIALIZER, "apps", "app");
    public final _Resource<OTComment> comments = new _Resource<OTComment>(OTComment.SERIALIZER, "comments", "comment");
    public final _Resource<OTDeal> deals = new _Resource<OTDeal>(OTDeal.SERIALIZER, "deals", "deal");
    public final _Resource<OTDevice> devices = new _Resource<OTDevice>(OTDevice.SERIALIZER, "devices", "device");
    public final _Resource<OTGroup> groups = new _Resource<OTGroup>(OTGroup.SERIALIZER, "groups", "group");
    public final _Resource<OTItem> items = new _Resource<OTItem>(OTItem.SERIALIZER, "items", "item");
    public final _Resource<OTLeaderboard> leaderboards = new _Resource<OTLeaderboard>(OTLeaderboard.SERIALIZER, "leaderboards", "leaderboard");
    public final _Resource<OTMember> members = new _Resource<OTMember>(OTMember.SERIALIZER, "members", "member");
    public final _Resource<OTPlayer> players = new _Resource<OTPlayer>(OTPlayer.SERIALIZER, "players", "player");
    public final _Resource<OTPost> posts = new _Resource<OTPost>(OTPost.SERIALIZER, "posts", "post");
    public final _Resource<OTProduct> products = new _Resource<OTProduct>(OTProduct.SERIALIZER, "products", "product");
    public final _Resource<OTProgress> progresses = new _Resource<OTProgress>(OTProgress.SERIALIZER, "progresses", "progress");
    public final _Resource<OTScore> scores = new _Resource<OTScore>(OTScore.SERIALIZER, "scores", "score");
    public final _Resource<OTTag> tags = new _Resource<OTTag>(OTTag.SERIALIZER, "tags", "tag");
    public final _Resource<OTTicket> tickets = new _Resource<OTTicket>(OTTicket.SERIALIZER, "tickets", "ticket");
    public final _Resource<OTUser> users = new _Resource<OTUser>(OTUser.SERIALIZER, "users", "user");

    //
    // users
    //

    public void setUserPassword(String password, String passwordNew, final OTClientCallback callback) {
        JSONObject data = OTJson.object("password", password, "passwordNew", passwordNew);
        _request(POST, "/users/me/password", data, callback);
    }

    public void getUserProfile(String userId, final OTClientCallback<OTUser> callback) {
        _request(GET, "/users/" + _me(userId) + "/profile", new JsonSingularCallback<OTUser>(OTUser.SERIALIZER, "user", callback));
    }

    public void setUserProfile(OTUser profile, final OTClientCallback<OTUser> callback) {
        JSONObject data = OTUser.SERIALIZER.toJson(profile);
        _request(PUT, "/users/me/profile", data, new JsonSingularCallback<OTUser>(OTUser.SERIALIZER, "user", callback));
    }

    public void getUserById(String userId, OTClientCallback<OTUser> callback) {
        users.load(_me(userId), callback);
    }

    public void getUserSummary(String userId, OTClientCallback<OTUserSummary> callback) {
        String path = "/users/" + _me(userId) + "/summary";
        _request(GET, path, new UserSummaryCallback(callback));
    }

    public void reportAbuseUser(String userId, String reason, String description, OTClientCallback callback) {
        String path = "/users/" + userId + "/abuse";
        JSONObject data = OTJson.object("reason", reason, "description", description);
        _request(POST, path, data, callback);
    }

    public void findUsers(String q, int skip, int limit, OTClientCallback<List<OTUser>> callback) {
        search(q, "user", skip, limit, new JsonPluralCallback<OTUser>(OTUser.SERIALIZER, "users", callback));
    }

    //
    // apps
    //

    public void getHotApps(int skip, int limit, OTClientCallback<List<OTApp>> callback) {
        String path = "/apps/featured/hot";
        Map<String, Object> params = OTUtils.params("skip", skip, "limit", limit);
        _request(GET, path, null, params, new JsonPluralCallback<OTApp>(OTApp.SERIALIZER, "apps", callback));
    }

    public void getNewApps(int skip, int limit, OTClientCallback<List<OTApp>> callback) {
        String path = "/apps/featured/new";
        Map<String, Object> params = OTUtils.params("skip", skip, "limit", limit);
        _request(GET, path, null, params, new JsonPluralCallback<OTApp>(OTApp.SERIALIZER, "apps", callback));
    }

    public void getAppById(String appId, OTClientCallback<OTApp> callback) {
        apps.load(_me(appId), callback);
    }

    public void getUserApps(String userId, OTClientCallback<List<OTApp>> callback) {
        String path = "/users/" + _me(userId) + "/apps";
        _request(GET, path, new JsonPluralCallback<OTApp>(OTApp.SERIALIZER, "apps", callback));
    }

    public void getFriendsApps(String userId, int skip, int limit, OTClientCallback<List<OTApp>> callback) {
        String path = "/users/" + _me(userId) + "/apps/friends";
        Map<String, Object> params = OTUtils.params("skip", skip, "limit", limit);
        _request(GET, path, null, params, new JsonPluralCallback<OTApp>(OTApp.SERIALIZER, "apps", callback));
    }

    public void getAppSummary(String appId, OTClientCallback<OTAppSummary> callback) {
        String path = "/apps/" + _me(appId) + "/summary";
        _request(GET, path, new AppSummaryCallback(callback));
    }

    public void reportAbuseApp(String appId, String reason, String description, OTClientCallback callback) {
        String path = "/apps/" + appId + "/abuse";
        JSONObject data = OTJson.object("reason", reason, "description", description);
        _request(POST, path, data, callback);
    }

    public void findApps(String q, int skip, int limit, OTClientCallback<List<OTApp>> callback) {
        search(q, "app", skip, limit, new JsonPluralCallback<OTApp>(OTApp.SERIALIZER, "apps", callback));
    }

    //
    // players
    //

    public void getPlayerProfile(String playerId, final OTClientCallback<OTPlayer> callback) {
        _request(GET, "/players/" + _me(playerId) + "/profile", new JsonSingularCallback<OTPlayer>(OTPlayer.SERIALIZER, "player", callback));
    }

    public void setPlayerProfile(OTPlayer profile, final OTClientCallback<OTPlayer> callback) {
        JSONObject data = OTPlayer.SERIALIZER.toJson(profile);
        _request(PUT, "/players/me/profile", data, new JsonSingularCallback<OTPlayer>(OTPlayer.SERIALIZER, "player", callback));
    }

    public void getPlayerById(String playerId, OTClientCallback<OTPlayer> callback) {
        players.load(_me(playerId), callback);
    }

    public void getPlayerSummary(String playerId, OTClientCallback<OTPlayerSummary> callback) {
        String path = "/players/" + _me(playerId) + "/summary";
        _request(GET, path, new PlayerSummaryCallback(callback));
    }

    //
    // leaderboards/scores
    //

    public void reportScore(String leaderboardId, int value, OTClientCallback<OTScore> callback) {
        OTScore score = new OTScore(currentPlayer.getId(), leaderboardId, value);
        scores.create(score, callback);
    }

    public void reportScore(String leaderboardId, int value, double lat, double lng, OTClientCallback<OTScore> callback) {
        OTScore score = new OTScore(currentPlayer.getId(), leaderboardId, value, lat, lng);
        scores.create(score, callback);
    }

    public void getAppLeaderboards(String appId, OTClientCallback<List<OTLeaderboard>> callback) {
        String path = "/apps/" + _me(appId) + "/leaderboards";
        _request(GET, path, new JsonPluralCallback<OTLeaderboard>(OTLeaderboard.SERIALIZER, "leaderboards", callback));
    }

    public void getAppLeaderboard(String appId, OTClientCallback<OTLeaderboard> callback) {
        String path = "/apps/" + _me(appId) + "/leaderboards/me";
        _request(GET, path, new JsonSingularCallback<OTLeaderboard>(OTLeaderboard.SERIALIZER, "leaderboard", callback));
    }

    public void getLeaderboardScores(String leaderboardId, int skip, int limit, OTClientCallback<OTLeaderboardScores> callback) {
        String path = "/leaderboards/" + _me(leaderboardId) + "/scores";
        Map<String, Object> params = OTUtils.params("skip", skip, "limit", limit);
        _request(GET, path, null, params, new LeaderboardScoresCallback(callback));
    }

    public void getLeaderboardScoresByPlayer(String leaderboardId, String playerId, int skip, int limit, OTClientCallback<OTLeaderboardScores> callback) {
        String path = "/leaderboards/" + _me(leaderboardId) + "/scores/players/" + _me(playerId);
        Map<String, Object> params = OTUtils.params("skip", skip, "limit", limit);
        _request(GET, path, null, params, new LeaderboardScoresCallback(callback));
    }

    public void getLeaderboardScoresByFriends(String leaderboardId, String userId, int skip, int limit, OTClientCallback<OTLeaderboardScores> callback) {
        String path = "/leaderboards/" + _me(leaderboardId) + "/scores/friends/" + _me(userId);
        Map<String, Object> params = OTUtils.params("skip", skip, "limit", limit);
        _request(GET, path, null, params, new LeaderboardScoresCallback(callback));
    }

    public void getLeaderboardScoresByNear(String leaderboardId, double lat, double lng, int radius, int skip, int limit, OTClientCallback<OTLeaderboardScores> callback) {
        String path = "/leaderboards/" + _me(leaderboardId) + "/scores/near/" + lat + "," + lng + "," + radius;
        Map<String, Object> params = OTUtils.params("skip", skip, "limit", limit);
        _request(GET, path, null, params, new LeaderboardScoresCallback(callback));
    }

    public void getLeaderboardSummary(String leaderboardId, OTClientCallback<OTLeaderboardSummary> callback) {
        String path = "/leaderboards/" + leaderboardId + "/summary";
        _request(GET, path, new LeaderboardSummaryCallback(callback));
    }

    //
    // achievements/progresses
    //

    public void reportProgress(String achievementId, int value, OTClientCallback<OTProgress> callback) {
        OTProgress progress = new OTProgress(currentPlayer.getId(), achievementId, value);
        progresses.create(progress, callback);
    }

    public void getAppAchievements(String appId, OTClientCallback<List<OTAchievement>> callback) {
        String path = "/apps/" + _me(appId) + "/achievements";
        _request(GET, path, new JsonPluralCallback<OTAchievement>(OTAchievement.SERIALIZER, "achievements", callback));
    }

    public void getPlayerAchievements(String playerId, OTClientCallback<List<OTAchievement>> callback) {
        String path = "/players/" + _me(playerId) + "/achievements";
        _request(GET, path, new JsonPluralCallback<OTAchievement>(OTAchievement.SERIALIZER, "achievements", callback));
    }

    public void getAchievementProgresses(String achievementId, int progress, int skip, int limit, OTClientCallback<OTAchievementProgresses> callback) {
        String path = "/achievements/" + achievementId + "/progresses";
        Map<String, Object> params = OTUtils.params("progress", progress, "skip", skip, "limit", limit);
        _request(GET, path, null, params, new AchievementProgressesCallback(callback));
    }


    public void getAchievementProgressesByFriends(String achievementId, int progress, String userId, int skip, int limit, OTClientCallback<OTAchievementProgresses> callback) {
        String path = "/achievements/" + achievementId + "/progresses/friends/" + _me(userId);
        Map<String, Object> params = OTUtils.params("progress", progress, "skip", skip, "limit", limit);
        _request(GET, path, null, params, new AchievementProgressesCallback(callback));
    }

    public void getAchievementSummary(String achievementId, OTClientCallback<OTAchievementSummary> callback) {
        String path = "/achievements/" + achievementId + "/summary";
        _request(GET, path, new AchievementSummaryCallback(callback));
    }

    //
    // groups/members
    //

    //
    // relations/friends
    //

    //
    // properties
    //

    public void _setProperty(String ownerType, String ownerId, String key, String value, OTClientCallback callback) {
        String path = "/" + ownerType + "s/" + ownerId + "/properties/" + key;
        JSONObject data = OTJson.object("value", value);
        _request(PUT, path, data, callback);
    }

    public void _getProperty(String ownerType, String ownerId, String key, OTClientCallback<String> callback) {
        String path = "/" + ownerType + "s/" + ownerId + "/properties/" + key;
        _request(GET, path, new JsonTransformCallback<String>(callback) {
            @Override
            protected String transform(JSONObject data) {
                return data.optString("value");
            }
        });
    }

    public void _removeProperty(String ownerType, String ownerId, String key, OTClientCallback callback) {
        String path = "/" + ownerType + "s/" + ownerId + "/properties/" + key;
        _request(DELETE, path, callback);
    }

    public void _clearProperty(String ownerType, String ownerId, OTClientCallback callback) {
        String path = "/" + ownerType + "s/" + ownerId + "/properties";
        _request(DELETE, path, callback);
    }

    //---------------------------------------------------------------

    private abstract static class TransformCallback<FROM, TO> implements OTClientCallback<FROM> {
        private final OTClientCallback<TO> callback;

        public TransformCallback(OTClientCallback<TO> callback) {
            this.callback = callback;
        }

        protected abstract TO transform(FROM data);

        @Override
        public void onSuccess(FROM data) {
            _passSuccess(callback, transform(data));
        }

        @Override
        public void onError(OTClientError error) {
            _passError(callback, error);
        }
    }

    //---------------------------------------------------------------

    private abstract static class JsonTransformCallback<TO> extends TransformCallback<JSONObject, TO> {
        public JsonTransformCallback(OTClientCallback<TO> delegate) {
            super(delegate);
        }
    }

    private static class JsonSingularCallback<T> extends JsonTransformCallback<T> {
        private final OTJsonSerializer<T> serializer;
        private final String singular;

        public JsonSingularCallback(OTJsonSerializer<T> serializer, String singular, OTClientCallback<T> delegate) {
            super(delegate);
            this.serializer = serializer;
            this.singular = singular;
        }

        @Override
        public T transform(JSONObject data) {
            return serializer.fromJson(data.optJSONObject(singular));
        }
    }

    private static class JsonPluralCallback<T> extends JsonTransformCallback<List<T>> {
        private final OTJsonSerializer<T> serializer;
        private final String plural;

        public JsonPluralCallback(OTJsonSerializer<T> serializer, String plural, OTClientCallback<List<T>> delegate) {
            super(delegate);
            this.serializer = serializer;
            this.plural = plural;
        }

        @Override
        public List<T> transform(JSONObject data) {
            return serializer.fromJson(data.optJSONArray(plural));
        }
    }

    private static class UserSummaryCallback extends JsonTransformCallback<OTUserSummary> {
        public UserSummaryCallback(OTClientCallback delegate) {
            super(delegate);
        }

        @Override
        protected OTUserSummary transform(JSONObject json) {
            return OTUserSummary.fromJson(json);
        }
    }

    private static class AppSummaryCallback extends JsonTransformCallback<OTAppSummary> {
        public AppSummaryCallback(OTClientCallback delegate) {
            super(delegate);
        }

        @Override
        protected OTAppSummary transform(JSONObject json) {
            return OTAppSummary.fromJson(json);
        }
    }

    private static class PlayerSummaryCallback extends JsonTransformCallback<OTPlayerSummary> {
        public PlayerSummaryCallback(OTClientCallback delegate) {
            super(delegate);
        }

        @Override
        protected OTPlayerSummary transform(JSONObject json) {
            return OTPlayerSummary.fromJson(json);
        }
    }

    private static class LeaderboardScoresCallback extends JsonTransformCallback<OTLeaderboardScores> {

        private LeaderboardScoresCallback(OTClientCallback<OTLeaderboardScores> delegate) {
            super(delegate);
        }

        @Override
        protected OTLeaderboardScores transform(JSONObject json) {
            return OTLeaderboardScores.fromJson(json);
        }
    }

    private static class LeaderboardSummaryCallback extends JsonTransformCallback<OTLeaderboardSummary> {
        public LeaderboardSummaryCallback(OTClientCallback delegate) {
            super(delegate);
        }

        @Override
        protected OTLeaderboardSummary transform(JSONObject json) {
            return OTLeaderboardSummary.fromJson(json);
        }
    }

    private static class AchievementProgressesCallback extends JsonTransformCallback<OTAchievementProgresses> {
        private AchievementProgressesCallback(OTClientCallback<OTAchievementProgresses> delegate) {
            super(delegate);
        }

        @Override
        protected OTAchievementProgresses transform(JSONObject json) {
            return OTAchievementProgresses.fromJson(json);
        }
    }

    private static class AchievementSummaryCallback extends JsonTransformCallback<OTAchievementSummary> {
        public AchievementSummaryCallback(OTClientCallback delegate) {
            super(delegate);
        }

        @Override
        protected OTAchievementSummary transform(JSONObject json) {
            return OTAchievementSummary.fromJson(json);
        }
    }

}
