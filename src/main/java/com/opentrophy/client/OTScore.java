package com.opentrophy.client;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OTScore {

    private final String id;
    private final OTPlayer player;
    private final String playerId;
    private final String leaderboardId;
    private final int value;
    private final double lat;
    private final double lng;
    private final Date createdAt;

    public OTScore(String playerId, String leaderboardId, int value, double lat, double lng) {
        this.id = null;
        this.player = null;
        this.playerId = playerId;
        this.leaderboardId = leaderboardId;
        this.value = value;
        this.lat = lat;
        this.lng = lng;
        this.createdAt = new Date();
    }

    public OTScore(String playerId, String leaderboardId, int value) {
        this(playerId, leaderboardId, value, 0, 0);
    }

    private OTScore(JSONObject json) {
        id = json.optString("_id");
        if (json.opt("player") instanceof String) {
            player = null;
            playerId = json.optString("player");
        } else {
            player = OTPlayer.SERIALIZER.fromJson(json.optJSONObject("player"));
            playerId = player.getId();
        }
        leaderboardId = json.optString("leaderboard");
        value = json.optInt("value");
        if (json.has("loc")) {
            JSONArray locArray = json.optJSONArray("loc");
            lng = locArray.optDouble(0); // NOTE: lng first in mongodb!
            lat = locArray.optDouble(1);
        } else {
            lat = 0;
            lng = 0;
        }
        createdAt = OTUtils.toDate(json.optString("createdAt"));
    }

    //
    // getters
    //

    public String getId() {
        return id;
    }

    public OTPlayer getPlayer() {
        return player;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getLeaderboardId() {
        return leaderboardId;
    }

    public int getValue() {
        return value;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    //
    //
    //

    @Override
    public String toString() {
        return "[OTScore:player=" + playerId + ",leaderboard=" + leaderboardId + ",value=" + value + "]";
    }

    public static final OTJsonSerializer<OTScore> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTScore>() {
        @Override
        public JSONObject toJson(final OTScore obj) {
            return OTJson.object("_id", obj.id, "player", obj.playerId, "leaderboard", obj.leaderboardId,
                    "value", obj.value, "loc", OTJson.array(obj.lng, obj.lat));
        }

        @Override
        public OTScore fromJson(final JSONObject json) {
            return new OTScore(json);
        }
    };
}
