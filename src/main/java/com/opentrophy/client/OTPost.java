package com.opentrophy.client;

import org.json.JSONObject;

public class OTPost {

    private final String id;

    private OTPost(final JSONObject json) {
        id = json.optString("_id");
    }

    public String getId() {
        return id;
    }
//
    //
    //

    @Override
    public String toString() {
        return "[OPage:id=" + id + "]";
    }

    public static final OTJsonSerializer<OTPost> SERIALIZER = new OTJsonSerializer.WithArraySupport<OTPost>() {
        @Override
        public JSONObject toJson(final OTPost obj) {
            return OTJson.object("_id", obj.id);
        }

        @Override
        public OTPost fromJson(final JSONObject json) {
            return new OTPost(json);
        }
    };
}
