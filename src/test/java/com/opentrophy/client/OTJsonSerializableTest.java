package com.opentrophy.client;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class OTJsonSerializableTest {

    public static class Foo {
        public final String bar;

        public Foo(String bar) {
            this.bar = bar;
        }

        @Override
        public boolean equals(Object foo) {
            return (foo != null) && (foo instanceof Foo) && bar.equals(((Foo) foo).bar);
        }

        public static final OTJsonSerializer<Foo> SERIALIZER = new OTJsonSerializer.WithArraySupport<Foo>() {
            @Override
            public JSONObject toJson(Foo obj) {
                return OTJson.object("bar", obj.bar);
            }

            @Override
            public Foo fromJson(JSONObject json) {
                return new Foo(json.optString("bar"));
            }
        };
    }

    @Test
    public void testToJson() {
        Foo foo = new Foo("hello");
        JSONObject json = Foo.SERIALIZER.toJson(foo);
        System.out.println("json:" + json.toString());
        Foo foo2 = Foo.SERIALIZER.fromJson(json);
        System.out.println(OTUtils.dump(foo2));
        Assert.assertEquals(foo2, foo);
    }

    @Test
    public void testToJsonArray() {
        List<Foo> foos = new ArrayList<Foo>();
        foos.add(new Foo("hello"));
        foos.add(new Foo("world"));
        JSONArray json = Foo.SERIALIZER.toJson(foos);
        System.out.println("json:" + json.toString());
        List<Foo> foos2 = Foo.SERIALIZER.fromJson(json);
        System.out.println(OTUtils.dump(foos2));
        Assert.assertEquals(foos2, foos);
    }
}
