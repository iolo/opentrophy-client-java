package com.opentrophy.client;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class OTJsonTest {

    @Test
    public void testObject() {
        JSONObject json = OTJson.object("string", "string", "int", 123, "double", 123.456, "true", true, "false", false, "null", null);
        Assert.assertNotNull(json);
        Assert.assertEquals("string", json.optString("string"));
        Assert.assertEquals(123, json.optInt("int"));
        Assert.assertEquals(123.456, json.optDouble("double"), 1e-15);
        Assert.assertEquals(true, json.optBoolean("true"));
        Assert.assertEquals(false, json.optBoolean("false"));
        Assert.assertEquals(null, json.opt("null"));
    }

    @Test
    public void testArray() {
        JSONArray json = OTJson.array("string", 123, 123.456, true, false, null);
        Assert.assertNotNull(json);
        Assert.assertEquals("string", json.optString(0));
        Assert.assertEquals(123, json.optInt(1));
        Assert.assertEquals(123.456, json.optDouble(2), 1e-15);
        Assert.assertEquals(true, json.optBoolean(3));
        Assert.assertEquals(false, json.optBoolean(4));
        Assert.assertEquals(null, json.opt(5));
    }

}
