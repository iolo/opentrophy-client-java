package com.opentrophy.client;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

public class OTClientTest {

    static {
        System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "TRACE");
    }

    String key = "519f771c2721470fd9000005";//app1
    String secret = "test";
    String connectUrl = "http://localhost:3000/auth/bearer";
    String apiUrl = "http://localhost:3000/api/v1";
    OTClientConfig config = new OTClientConfig(key, secret, connectUrl, apiUrl);
    OTClient client = new OTClient(config);

    @Before
    public void setUp() throws Exception {
        final CountDownLatch lock = new CountDownLatch(1);

        client.connect(new OTClientCallback<OTApp>() {
            @Override
            public void onSuccess(OTApp data) {
                System.out.println(data);
                lock.countDown();
            }

            @Override
            public void onError(OTClientError error) {
                System.out.println(error);
                Assert.fail(error.toString());
            }
        });

        lock.await();
    }

    @After
    public void tearDown() {
        client.disconnect(null);
    }

    @Test
    public void testEcho() throws Exception {
        final CountDownLatch lock = new CountDownLatch(1);

        client.echo("GET", OTJson.object("hello", "world"), OTUtils.params("foo", "bar"), new OTClientCallback<JSONObject>() {
            @Override
            public void onSuccess(JSONObject data) {
                System.out.println(data);
                Assert.assertNotNull(data);
                lock.countDown();
            }

            @Override
            public void onError(OTClientError error) {
                System.out.println(error);
                Assert.fail(error.toString());
            }
        });

        lock.await();
    }

    @Test
    public void testPing() throws Exception {
        final CountDownLatch lock = new CountDownLatch(1);

        client.ping(new OTClientCallback() {
            @Override
            public void onSuccess(Object data) {
                System.out.println(data);
                Assert.assertNull(data);
                //Assert.assertNotNull(data);
                lock.countDown();
            }

            @Override
            public void onError(OTClientError error) {
                System.out.println(error);
                Assert.fail(error.toString());
            }
        });

        lock.await();
    }

    @Test
    public void testSignin() throws Exception {
        final CountDownLatch lock = new CountDownLatch(1);

        client.signin(new OTSigninInfo("local", "u1", "u1", false), new OTClientCallback<OTPlayer>() {
            @Override
            public void onSuccess(OTPlayer data) {
                System.out.println(data);
                Assert.assertNotNull(data);
                lock.countDown();
            }

            @Override
            public void onError(OTClientError error) {
                System.out.println(error);
                Assert.fail(error.toString());
            }
        });

        lock.await();
    }

    @Test
    public void testRESTfulResources() throws Exception {
        Assert.assertNotNull(client.achievements);
        Assert.assertNotNull(client.activities);
        Assert.assertNotNull(client.apps);
        Assert.assertNotNull(client.comments);
        Assert.assertNotNull(client.deals);
        Assert.assertNotNull(client.devices);
        Assert.assertNotNull(client.groups);
        Assert.assertNotNull(client.items);
        Assert.assertNotNull(client.leaderboards);
        Assert.assertNotNull(client.members);
        Assert.assertNotNull(client.players);
        Assert.assertNotNull(client.posts);
        Assert.assertNotNull(client.products);
        Assert.assertNotNull(client.progresses);
        Assert.assertNotNull(client.scores);
        Assert.assertNotNull(client.tags);
        Assert.assertNotNull(client.tickets);
        Assert.assertNotNull(client.users);
    }
}
